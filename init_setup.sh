#!/bin/bash

# This script handles the initial asset extraction for English Shindou Super Mario 64.
# Run in the root project folder.

set -e

# Extract SH assets

python3 extract_assets.py sh

python3 extract_assets.py us

# Finally, copy assets for M64 sequences to the US folder.

rm -rf sound/sequences/us
cp -r sound/sequences/sh/ sound/sequences/us/

