#include <ultra64.h>
#include "sm64.h"
#include "surface_terrains.h"
#include "geo_commands.h"

#include "make_const_nonconst.h"

// Note: This bin does not use bin IDs, unlike the other segmented bins.

#include "bubble/model.inc.c"

#include "walk_smoke/model.inc.c"

#include "burn_smoke/model.inc.c"

#include "stomp_smoke/model.inc.c"

#include "water_wave/model.inc.c"

#include "sparkle/model.inc.c"

#include "water_splash/model.inc.c"

#include "white_particle_small/model.inc.c"

#include "sparkle_animation/model.inc.c"

// This hack fixes yoshimilkman's mario model to use environment for solid coloring over primitive,
// as primitive color is used for rgfx environment lighting.

#undef gsDPSetPrimColor

#define gsDPSetPrimColor(x, y, r, g, b, a) gsDPSetEnvColor(r, g, b, a)

#undef G_CCMUX_PRIMITIVE
#define G_CCMUX_PRIMITIVE G_CCMUX_ENVIRONMENT

#include "mario/model.inc.c"