#include <ultra64.h>

#include "types.h"
#include "audio/external.h"
#include "engine/math_util.h"
#include "game/area.h"
#include "game/game_init.h"
#include "game/level_update.h"
#include "game/main.h"
#include "game/memory.h"
#include "game/print.h"
#include "game/save_file.h"
#include "game/sound_init.h"
#include "game/rumble_init.h"
#include "level_table.h"
#include "seq_ids.h"
#include "sm64.h"
#include "title_screen.h"

#include "game/segment2.h"
#include "game/rgfx_hud.h"

/**
 * @file title_screen.c
 * This file implements how title screen functions.
 * That includes playing demo sequences, introduction screens
 * and a level select used for testing purposes.
 */

#define STUB_LEVEL(textname, _1, _2, _3, _4, _5, _6, _7, _8) textname,
#define DEFINE_LEVEL(textname, _1, _2, _3, _4, _5, _6, _7, _8, _9, _10) textname,

static char sLevelSelectStageNames[64][16] = {
#include "levels/level_defines.h"
};
#undef STUB_LEVEL
#undef DEFINE_LEVEL

s32 gDisplayFileSelect = FALSE;

/**
 * Level select intro function, updates the selected stage
 * count if an input was received. signals the stage to be started
 * or the level select to be exited if start or the quit combo is pressed.
 */
s16 intro_level_select(void) {
    s32 stageChanged = FALSE;

    // perform the ID updates per each button press.
    // runs into a loop so after a button is pressed
    // stageChanged goes back to FALSE
    if (gPlayer1Controller->buttonPressed & A_BUTTON) {
        ++gCurrLevelNum, stageChanged = TRUE;
    }
    if (gPlayer1Controller->buttonPressed & B_BUTTON) {
        --gCurrLevelNum, stageChanged = TRUE;
    }
    if (gPlayer1Controller->buttonPressed & U_JPAD) {
        --gCurrLevelNum, stageChanged = TRUE;
    }
    if (gPlayer1Controller->buttonPressed & D_JPAD) {
        ++gCurrLevelNum, stageChanged = TRUE;
    }
    if (gPlayer1Controller->buttonPressed & L_JPAD) {
        gCurrLevelNum -= 10, stageChanged = TRUE;
    }
    if (gPlayer1Controller->buttonPressed & R_JPAD) {
        gCurrLevelNum += 10, stageChanged = TRUE;
    }

    // if the stage was changed, play the sound for changing a stage.
    if (stageChanged) {
        play_sound(SOUND_GENERAL_LEVEL_SELECT_CHANGE, gGlobalSoundSource);
    }

    if (gCurrLevelNum > LEVEL_MAX) {
        gCurrLevelNum = LEVEL_MIN; // exceeded max. set to min.
    }

    if (gCurrLevelNum < LEVEL_MIN) {
        gCurrLevelNum = LEVEL_MAX; // exceeded min. set to max.
    }

    // Use file 4 and last act as a test
    gCurrSaveFileNum = 4;
    gCurrActNum = 6;

    print_text_centered(160, 80, "SELECT STAGE");
    print_text_centered(160, 30, "PRESS START BUTTON");
    print_text_fmt_int(40, 60, "%2d", gCurrLevelNum);
    print_text(80, 60, sLevelSelectStageNames[gCurrLevelNum - 1]); // print stage name

#define QUIT_LEVEL_SELECT_COMBO (Z_TRIG | START_BUTTON | L_CBUTTONS | R_CBUTTONS)

    // start being pressed signals the stage to be started. that is, unless...
    if (gPlayer1Controller->buttonPressed & START_BUTTON) {
        // ... the level select quit combo is being pressed, which uses START. If this
        // is the case, quit the menu instead.
        if (gPlayer1Controller->buttonDown == QUIT_LEVEL_SELECT_COMBO) {
            gDebugLevelSelect = FALSE;
            return -1;
        }
        play_sound(SOUND_MENU_STAR_SOUND, gGlobalSoundSource);
        return gCurrLevelNum;
    }
    return 0;
}

static s32 menuSelect = 0;

extern void fade_channel_volume_scale(u8 player, u8 channelIndex, u8 targetScale, u16 fadeDuration);

#define SOUND_MENU_RGFX_CONFIRM_BIG                                                                    \
    SOUND_ARG_LOAD(SOUND_BANK_MENU, 64, 0xFF, SOUND_CONSTANT_FREQUENCY | SOUND_DISCRETE)
#define SOUND_MENU_RGFX_CONFIRM_SMALL                                                                  \
    SOUND_ARG_LOAD(SOUND_BANK_MENU, 65, 0xF7, SOUND_CONSTANT_FREQUENCY | SOUND_DISCRETE)
#define SOUND_MENU_RGFX_EXIT                                                                           \
    SOUND_ARG_LOAD(SOUND_BANK_MENU, 66, 0xF7, SOUND_CONSTANT_FREQUENCY | SOUND_DISCRETE)
#define SOUND_MENU_RGFX_SCROLL                                                                         \
    SOUND_ARG_LOAD(SOUND_BANK_MENU, 67, 0xF2, SOUND_CONSTANT_FREQUENCY | SOUND_DISCRETE)
#define SOUND_MENU_RGFX_SELECT                                                                         \
    SOUND_ARG_LOAD(SOUND_BANK_MENU, 68, 0xF2, SOUND_CONSTANT_FREQUENCY | SOUND_DISCRETE)

static s32 play_select_sound() {
    play_sound(SOUND_MENU_RGFX_SELECT, gGlobalSoundSource);
    return 1;
}

#define TITLE_FADE_VOLUME 96
#define TITLE_FADE_TIME 16

s32 yPos[5] = { 64, 64, 64, 64, 156 };        // current
const s32 yPosInit[5] = { 64, 64, 64, 64, 156 }; // selected
const s32 yPos2[5] = { 56, 56, 56, 56, 148 }; // selected
s32 optionsOpenedOffset = 0;              // Opens to 64

void render_file_button(u8 index, u8 selected) {
    RgfxHud *hud;
    u8 colors[4];
    char buf[64];
    const char *names = "A\00B\00C\00D\00";
    u8 *(*hudLUT)[58] = segmented_to_virtual(&main_hud_lut);
    s32 i;

    if (selected) {
        colors[0] = 0xFA;
        colors[1] = 0xCD;
        colors[2] = 0x00;
        colors[3] = 192;
    } else {
        colors[0] = 0x1D;
        colors[1] = 0x1D;
        colors[2] = 0x1D;
        colors[3] = 128;
    }

    if (index != menuSelect) {
        yPos[index] =
            approach_s32(yPos[index], yPosInit[index], 2, 2); // make the selected box "jump"
    } else {
        yPos[index] =
            approach_s32(yPos[menuSelect], yPos2[menuSelect], 2, 2); // make the selected box "jump"
    }

    if ((hud = alloc_hud_command(0, 3)) != NULL) {
        hud[0] =
            rgfx_create_window(&names[index * 2], 28 + (68 * index), yPos[index], 92 + (68 * index),
                            yPos[index] + 64, colors[0], colors[1], colors[2], colors[3]);
        if (save_file_exists(index)) {
            hud[1] = rgfx_create_sprite(&hud[0], (*hudLUT)[53], 8, 32, 16, 16);
            sprintf(buf, "x  %d", save_file_get_total_star_count(index, COURSE_MIN - 1, COURSE_MAX - 1));
            hud[2] = rgfx_create_text(&hud[1], 18, 4, buf);
        } else {
            hud[1] = rgfx_create_text(&hud[0], 16, 36, "NEW");
            hud[2] = rgfx_none_command();
        }
    }
}

s32 gScaleMarioLogoBackwards = FALSE;

/**
 * Title screen function.
 */
s32 intro_regular(void) {
    u8 colors[4], i, j;
    s32 level = LEVEL_NONE;
    RgfxHud *hud;
    char buf[64];

    if (gDisplayFileSelect) {
        fade_channel_volume_scale(0, 0, TITLE_FADE_VOLUME, TITLE_FADE_TIME);

        fade_channel_volume_scale(0, 1, TITLE_FADE_VOLUME, TITLE_FADE_TIME);
        fade_channel_volume_scale(0, 2, TITLE_FADE_VOLUME, TITLE_FADE_TIME);
        fade_channel_volume_scale(0, 3, TITLE_FADE_VOLUME, TITLE_FADE_TIME);
        fade_channel_volume_scale(0, 4, TITLE_FADE_VOLUME, TITLE_FADE_TIME);

        fade_channel_volume_scale(0, 6, TITLE_FADE_VOLUME, TITLE_FADE_TIME);
        fade_channel_volume_scale(0, 7, TITLE_FADE_VOLUME, TITLE_FADE_TIME);
        fade_channel_volume_scale(0, 8, TITLE_FADE_VOLUME, TITLE_FADE_TIME);
        fade_channel_volume_scale(0, 9, TITLE_FADE_VOLUME, TITLE_FADE_TIME);
        fade_channel_volume_scale(0, 10, TITLE_FADE_VOLUME, TITLE_FADE_TIME);
        fade_channel_volume_scale(0, 11, TITLE_FADE_VOLUME, TITLE_FADE_TIME);
        fade_channel_volume_scale(0, 12, TITLE_FADE_VOLUME, TITLE_FADE_TIME);


    if (gDisplayFileSelect == RGFX_TITLE_MODE_FILE_SELECT) { // display mode specific (files / options)
        print_text_centered(160, 200, "SELECT FILE");
        optionsOpenedOffset = approach_s32(optionsOpenedOffset, 0, 16, 16);

        if (optionsOpenedOffset < 8) {
            for (i = 0; i < 4; i++) {
            if (i == menuSelect) {
                j = TRUE;
            } else {
                j = FALSE;
            }
                render_file_button(i, j);
            }
        }

        if (menuSelect == 4) {
            yPos[4] = approach_s32(yPos[4], yPos2[4], 2, 2);
        } else {
            yPos[4] = approach_s32(yPos[4], yPosInit[4], 2, 2);
        }
        j = 1;
    } else {
        print_text_centered(160, 200, "GAME SETTINGS");
        j = 2;
        optionsOpenedOffset = approach_s32(optionsOpenedOffset, 92, 12, 12);
    }
        
        if (menuSelect == 4 && gDisplayFileSelect == RGFX_TITLE_MODE_FILE_SELECT) {
            colors[0] = 0xFA;
            colors[1] = 0xCD;
            colors[2] = 0x00;
            colors[3] = 172;
        } else {
            colors[0] = 0x1D;
            colors[1] = 0x1D;
            colors[2] = 0x1D;
            colors[3] = 128;
        }

        if ((hud = alloc_hud_command(0, j)) != NULL) {
            // HUD commands

            hud[0] = rgfx_create_window("OPTIONS", 28, yPos[4] - optionsOpenedOffset, 294, yPos[4] + 52, colors[0], colors[1],
                        colors[2], colors[3]);

            if (gDisplayFileSelect == RGFX_TITLE_MODE_OPTIONS) {
                hud[1] = rgfx_create_text(&hud[0], 120, 32, "- Widescreen Mode OFF -");
            }


        // finally handle input

            if (gDisplayFileSelect == RGFX_TITLE_MODE_FILE_SELECT) {
                if (gPlayer1Controller->buttonPressed & START_BUTTON) {
                    if (menuSelect < 4) {
                        play_sound(SOUND_MENU_RGFX_CONFIRM_BIG, gGlobalSoundSource);
    #if ENABLE_RUMBLE
                        queue_rumble_data(60, 70);
                        func_sh_8024C89C(1);
    #endif
                        if (gDebugLevelSelect) {
                            level = 101;
                        } else {
                            level = LEVEL_CASTLE_GROUNDS;
                        }
                        gCurrSaveFileNum = menuSelect + 1;
                    } else {
                        play_sound(SOUND_MENU_RGFX_CONFIRM_SMALL, gGlobalSoundSource);
                        gDisplayFileSelect = RGFX_TITLE_MODE_OPTIONS;
                    }
                    } else if (menuSelect < 4 && gPlayer1Controller->rawStickY < -60) {
                        menuSelect = 4;
                        play_select_sound();
                    } else if (menuSelect > 3 && gPlayer1Controller->rawStickY > 60) {
                            menuSelect = 0;
                            play_select_sound();
                    } else if (gPlayer1Controller->rawStickX > 60 && !(gGlobalTimer % 4) && play_select_sound() && ++menuSelect > 3) {
                            menuSelect = 0;
                    } else if (gPlayer1Controller->rawStickX < -60 && !(gGlobalTimer % 4) && play_select_sound() &&--menuSelect < 0) {
                            menuSelect = 3;
                    }
                } else if (gDisplayFileSelect == RGFX_TITLE_MODE_OPTIONS) {
                    if (gPlayer1Controller->buttonPressed & B_BUTTON) {
                        gDisplayFileSelect = RGFX_TITLE_MODE_FILE_SELECT;
                        play_sound(SOUND_MENU_RGFX_EXIT, gGlobalSoundSource);
                    }
                }
        }

    } else {
        fade_channel_volume_scale(0, 0, 0, 1);
        fade_channel_volume_scale(0, 6, 0, 1);
        fade_channel_volume_scale(0, 7, 0, 1);
        fade_channel_volume_scale(0, 8, 0, 1);
        fade_channel_volume_scale(0, 9, 0, 1);
        fade_channel_volume_scale(0, 10, 0, 1);
        fade_channel_volume_scale(0, 11, 0, 1);
        fade_channel_volume_scale(0, 12, 0, 1);
        print_intro_text();
        if (gPlayer1Controller->buttonPressed & START_BUTTON) {
            gScaleMarioLogoBackwards = TRUE;
            play_sound(SOUND_MENU_STAR_SOUND, gGlobalSoundSource);
#if ENABLE_RUMBLE
            queue_rumble_data(60, 70);
            func_sh_8024C89C(1);
#endif
            gDisplayFileSelect = TRUE;
            
        }
    }

    return level;
}

/**
 * Plays the casual "It's a me mario" when the game stars.
 */
s32 intro_play_its_a_me_mario(void) {
    set_background_music(0, SEQ_SOUND_PLAYER, 0);
    play_sound(SOUND_MENU_COIN_ITS_A_ME_MARIO, gGlobalSoundSource);
    return 1;
}

/**
 * Update intro functions to handle title screen actions.
 * Returns a level ID after their criteria is met.
 */
s32 lvl_intro_update(s16 arg, UNUSED s32 unusedArg) {
    s32 retVar;

    switch (arg) {
        case LVL_INTRO_PLAY_ITS_A_ME_MARIO:
            retVar = intro_play_its_a_me_mario();
            break;
        case LVL_INTRO_REGULAR:
            retVar = intro_regular();
            break;
        case LVL_INTRO_GAME_OVER:
            retVar = intro_regular();
            break;
        case LVL_INTRO_LEVEL_SELECT:
            retVar = intro_level_select();
            break;
    }
    return retVar;
}
