#ifndef TITLE_SCREEN_H
#define TITLE_SCREEN_H

#include <PR/ultratypes.h>

#include "macros.h"

enum RgfxFileSelect {
    RGFX_TITLE_MODE_DISABLED,
    RGFX_TITLE_MODE_FILE_SELECT,
    RGFX_TITLE_MODE_OPTIONS
};

enum LevelScriptIntroArgs {
    LVL_INTRO_PLAY_ITS_A_ME_MARIO,
    LVL_INTRO_REGULAR,
    LVL_INTRO_GAME_OVER,
    LVL_INTRO_LEVEL_SELECT,
};

s32 lvl_intro_update(s16 arg, UNUSED s32 unusedArg);

extern s32 gDisplayFileSelect;

#endif // TITLE_SCREEN_H
