#ifndef RGFX_HUD_H
#define RGFX_HUD_H

#define RGFX_HUD_MAX_COMMANDS 128
#define RGFX_HUD_MAX_LAYERS 3

#define RGFX_HUD_TYPE_END 0
#define RGFX_HUD_TYPE_WINDOW 1
#define RGFX_HUD_TYPE_BUTTON 2
#define RGFX_HUD_TYPE_LISTITEM 3
#define RGFX_HUD_TYPE_SPRITE 4
#define RGFX_HUD_TYPE_TEXT 5
#define RGFX_HUD_TYPE_NONE 6

#define RGFX_SELECTED_ITEM_NONE -1

#define RGFX_SELECT_STATUS_SELECTED 2

#define RGFX_INPUT_ACTION_PREV 0
#define RGFX_INPUT_ACTION_NEXT 1

typedef struct {
    u8 type;
    u8 selectable;
    u8 status;
    s16 x, y;
    void *parent;
} RgfxBase;

typedef struct {
    RgfxBase base;
    char title[32];
    s16 x2, y2;
    u8 bgColor[4];
} RgfxWindow;

typedef struct {
    RgfxBase base;
    char title[32];
    s16 x2, y2;
    u8 bgColor[4];
    u8 bgColorSelect[4];
} RgfxButton;

typedef struct {
    RgfxBase base;
    char title[32];
} RgfxListItem;

typedef struct {
    RgfxBase base;
    char title[64];
} RgfxText;

typedef struct {
    RgfxBase base;
    u8 sX, sY;
    Texture *tex;
} RgfxSprite;

typedef union {
    RgfxBase base;
    RgfxWindow win;
    RgfxButton button;
    RgfxListItem item;
    RgfxText text;
    RgfxSprite sprite;
} RgfxHud;

#define RGFX_SELECT_HORIZONTAL 0
#define RGFX_SELECT_VERTICAL 1

typedef struct {
    u8 mode;
    u8 selection;
    RgfxHud *select[8];
} RgfxSelectionList;

RgfxHud *alloc_hud_command(u8 layer, u8 commands);
void rgfx_hud_update();

RgfxHud rgfx_end_command();
RgfxHud rgfx_create_window(char *title, s16 x, s16 y, s16 x2, s16 y2, u8 r, u8 g, u8 b, u8 a);
RgfxHud rgfx_create_button(RgfxHud *parent, s16 x, s16 y, char *label);
RgfxHud rgfx_create_text(RgfxHud *parent, s16 x, s16 y, char *label);
RgfxHud rgfx_create_sprite(RgfxHud *parent, Texture *t, s16 x, s16 y, s16 sX, s16 sY);
RgfxHud rgfx_none_command();


void drawSmallString_impl(Gfx**, int, int, const char*, int, int , int);

#define RGFX_PRINT_COLOR(x, y, str, r, g, b) \
        gDPPipeSync(gDisplayListHead++); \
        gDPSetCycleType(gDisplayListHead++, G_CYC_1CYCLE); \
        gDPSetRenderMode(gDisplayListHead++, G_RM_TEX_EDGE, G_RM_TEX_EDGE2); \
        gDPSetTexturePersp(gDisplayListHead++, G_TP_NONE); \
        gDPSetTextureFilter(gDisplayListHead++, G_TF_POINT); \
        gDPSetTextureLUT(gDisplayListHead++, G_TT_NONE); \
        drawSmallString_impl(&gDisplayListHead, x, y, str, r, g, b);

#define RGFX_PRINT(x, y, str) RGFX_PRINT_COLOR(x, y, str, 255, 255, 255);

#define RGFX_PRINT_COLOR_CHAINED(x, y, string, r, g, b) drawSmallString_impl(&gDisplayListHead, x, y, string, r, g, b);
#define RGFX_PRINT_CHAINED(x, y, string) RGFX_PRINT_COLOR_CHAINED(x, y, string, 255, 255, 255);

#endif
