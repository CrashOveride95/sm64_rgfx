#include <ultra64.h>
#include "sm64.h"
#include "rgfx_hud.h"
#include "game_init.h"
#include "segment2.h"
#include "level_update.h"
#include "audio/external.h"

static RgfxHud hudPool[RGFX_HUD_MAX_LAYERS][RGFX_HUD_MAX_COMMANDS] = {0};
static RgfxHud *commandListHead[RGFX_HUD_MAX_LAYERS] = { &hudPool[0], &hudPool[1], &hudPool[2]};

// thank you hackersm64

// This file is a modification of a file from https://github.com/danbolt/n64-jam-1, which was licensed under the MPL-2.0 License
// See the original repo for more details.

#define TEX_ASCII_START '!'
#define TAB_WIDTH 16

#define G_CC_TEXT PRIMITIVE, 0, TEXEL0, 0, 0, 0, 0, TEXEL0

// ------------------------------------------------------------------- PRINT

extern u8 fast_font[];

int computeS(unsigned char letter) {
    int idx = letter;  
    if (letter > 'z') {
        idx -= (3 + 2 + 3 + 1 + 3);
    } else if (letter > '^') {
        idx -= (2 + 3 + 1 + 3);
    } else if (letter > 'Z') {
        idx -= (3 + 1 + 3);
    } else if (letter > '?') {
        idx -= (1 + 3);
    } else if (letter > ';') {
        idx -= (3);
    }

    return (idx - TEX_ASCII_START) * 8;
}

static const u8 fast_text_font_kerning[] = {
    /* */ 2, /*!*/ 4, /*"*/ 5, /*#*/ 0, /*$*/ 0, /*%*/ 7, /*&*/ 7, /*'*/ 2, /*(*/ 5, /*)*/ 5, /***/ 0, /*+*/ 7, /*,*/ 3, /*-*/ 7, /*.*/ 3, /*/*/ 7,
    /*0*/ 7, /*1*/ 7, /*2*/ 7, /*3*/ 7, /*4*/ 7, /*5*/ 7, /*6*/ 7, /*7*/ 7, /*8*/ 7, /*9*/ 7, /*:*/ 3, /*;*/ 3, /*<*/ 0, /*=*/ 0, /*>*/ 0, /*?*/ 7,
    /*@*/ 0, /*A*/ 7, /*B*/ 7, /*C*/ 7, /*D*/ 7, /*E*/ 7, /*F*/ 7, /*G*/ 7, /*H*/ 7, /*I*/ 6, /*J*/ 5, /*K*/ 7, /*L*/ 7, /*M*/ 7, /*N*/ 7, /*O*/ 7,
    /*P*/ 7, /*Q*/ 7, /*R*/ 7, /*S*/ 7, /*T*/ 7, /*U*/ 7, /*V*/ 7, /*W*/ 7, /*X*/ 7, /*Y*/ 7, /*Z*/ 7, /*[*/ 0, /*\*/ 0, /*]*/ 0, /*^*/ 6, /*_*/ 0,
    /*`*/ 0, /*a*/ 6, /*b*/ 6, /*c*/ 6, /*d*/ 6, /*e*/ 6, /*f*/ 6, /*g*/ 6, /*h*/ 6, /*i*/ 2, /*j*/ 6, /*k*/ 5, /*l*/ 3, /*m*/ 6, /*n*/ 6, /*o*/ 6,
    /*p*/ 6, /*q*/ 6, /*r*/ 6, /*s*/ 6, /*t*/ 6, /*u*/ 6, /*v*/ 6, /*w*/ 6, /*x*/ 6, /*y*/ 6, /*z*/ 6, /*{*/ 0, /*|*/ 0, /*}*/ 0, /*~*/ 7,     
};

void drawSmallString_impl(Gfx **dl, int x, int y, const char* string, int r, int g, int b) {
    int i = 0;
    int xPos = x;
    int yPos = y;
    int s = 0;
    Gfx *dlHead = *dl;

    gDPLoadTextureBlock_4bS(dlHead++, fast_font, G_IM_FMT_IA, 672, 12, 0, G_TX_MIRROR | G_TX_WRAP, G_TX_MIRROR | G_TX_WRAP, G_TX_NOMASK, G_TX_NOMASK, G_TX_NOLOD, G_TX_NOLOD);
    gDPSetPrimColor(dlHead++, 0, 0, r, g, b, 255);
    gDPSetCombineMode(dlHead++, G_CC_TEXT, G_CC_TEXT);
    gDPPipeSync(dlHead++);

    while (string[i] != '\0') {
        unsigned int cur_char = string[i];

        if (cur_char == '\n') {
            xPos = x;
            yPos += 12;
            i++;
            continue;
        }

        if (cur_char == '\t') {
            int xDist = xPos - x + 1;
            int tabCount = (xDist + TAB_WIDTH - 1) / TAB_WIDTH;
            xPos = tabCount * TAB_WIDTH + x;
        } else {
            if (cur_char != ' ') {
                s = computeS(cur_char);
                gSPTextureRectangle(dlHead++, (xPos + 0) << 2, (yPos + 0) << 2, (xPos + 8) << 2, (yPos + 12) << 2, 0, s << 5, 0, 1 << 10, 1 << 10);
            }
            xPos += fast_text_font_kerning[cur_char - ' '];
        }

        i++;
    }
    gDPSetPrimColor(dlHead++, 0, 0, 255, 255, 255, 255);
    gDPPipeSync(dlHead++);

    *dl = dlHead;
}

// thank you fazana

// ------------------------------------------------------------------- BACKGROUND

#define BLANK 0, 0, 0, ENVIRONMENT, 0, 0, 0, ENVIRONMENT

static void prepare_blank_box(void) {
    gDPSetCombineMode(gDisplayListHead++, BLANK, BLANK);
}

static void finish_blank_box(void) {
    gDPPipeSync(gDisplayListHead++);
    gDPSetEnvColor(gDisplayListHead++, 255, 255, 255, 255);
    gSPDisplayList(gDisplayListHead++, dl_hud_img_end);
}

static void render_blank_box(s32 x1, s32 y1, s32 x2, s32 y2, u8 r, u8 g, u8 b, u8 a) {
	u32 temp;
	s32 cycleadd;

    if (x2 < x1) {
        temp = x2;
        x2 = x1;
        x1 = temp;
    }

    if (y2 < y1) {
        u32 temp = y2;
        y2 = y1;
        y1 = temp;
    }

    if (x1 < 0) x1 = 0;
    if (y1 < 0) y1 = 0;
    if (x2 > SCREEN_WIDTH) x2 = SCREEN_WIDTH;
    if (y2 > SCREEN_HEIGHT) y2 = SCREEN_HEIGHT;
    cycleadd = 0;
    gDPPipeSync(gDisplayListHead++);
    if (((absi(x1 - x2) % 4) == 0) && (a == 255)) {
        gDPSetCycleType(gDisplayListHead++, G_CYC_FILL);
        gDPSetRenderMode(gDisplayListHead++, G_RM_NOOP, G_RM_NOOP);
        cycleadd = 1;
    } else {
        gDPSetCycleType(gDisplayListHead++, G_CYC_1CYCLE);
        if (a == 255) {
            gDPSetRenderMode(gDisplayListHead++, G_RM_OPA_SURF, G_RM_OPA_SURF2);
        } else {
            gDPSetRenderMode(gDisplayListHead++, G_RM_XLU_SURF, G_RM_XLU_SURF2);
        }
        cycleadd = 0;
    }

    gDPSetFillColor(gDisplayListHead++, (GPACK_RGBA5551(r, g, b, 1) << 16) | GPACK_RGBA5551(r, g, b, 1));
    gDPSetEnvColor(gDisplayListHead++, r, g, b, a);
    gDPFillRectangle(gDisplayListHead++, x1, y1, x2 - cycleadd, y2 - cycleadd);
    gDPPipeSync(gDisplayListHead++);
}

// ------------------------------------------------------------------- MISC

// thank you goddard :)

static void internal_strcpy(char *dst, const char *src) {
    while ((*dst++ = *src++)) {
        ;
    }
}

static char *internal_strcat(char *dst, const char *src) {
    while (*dst++) {
        ;
    }

    if (*src) {
        dst--;
        while ((*dst++ = *src++)) {
            ;
        }
    }

    return --dst;
}

static s16 get_true_position_x(RgfxHud *h, s16 offset) {
    s16 ret = 0;
    if (h->base.parent != NULL) {
        ret = get_true_position_x(h->base.parent, offset + h->base.x);
    } else {
        ret = h->base.x + offset;
    }
    return ret;
}

static s16 get_true_position_y(RgfxHud *h, s16 offset) {
    s16 ret = 0;
    if (h->base.parent != NULL) {
        ret = get_true_position_y(h->base.parent, offset + h->base.y);
    } else {
        ret = h->base.y + offset;
    }
    return ret;
}

static void render_texture_rectangle(s32 x, s32 y, s32 sX, s32 sY, u8 *texture) {
    gDPPipeSync(gDisplayListHead++);
    gDPSetTextureImage(gDisplayListHead++, G_IM_FMT_RGBA, G_IM_SIZ_16b, 1, texture);
    gSPDisplayList(gDisplayListHead++, &dl_hud_img_load_tex_block);
    gSPTextureRectangle(gDisplayListHead++, x << 2, y << 2, (x + sX - 1) << 2, (y + sY - 1) << 2, G_TX_RENDERTILE, 0, 0, 4 << 10, 1 << 10);
}

// PUBLIC FUNCTIONS

RgfxHud *alloc_hud_command(u8 layer, u8 commands) {
    RgfxHud *ret = commandListHead[layer];
    if (commandListHead[layer] + commands > &hudPool[layer][RGFX_HUD_MAX_COMMANDS]) {
        return NULL;
    }
    commandListHead[layer] = commandListHead[layer] + commands;
    return ret;
}

void rgfx_hud_init() {
    commandListHead[0] = &hudPool[0][0];
    commandListHead[1] = &hudPool[1][0];
    commandListHead[2] = &hudPool[2][0];
}

#define CURR_ITEM hudPool[i][j]

void rgfx_hud_update() {
    s16 x, y; // generic
    s16 i, j;

    *commandListHead[0] = rgfx_end_command();
    *commandListHead[1] = rgfx_end_command();
    *commandListHead[2] = rgfx_end_command();


    for (i = 0; i < RGFX_HUD_MAX_LAYERS; i++) {
        for (j = 0; j < RGFX_HUD_MAX_COMMANDS; j++) {
            switch (hudPool[i][j].base.type) {
                case RGFX_HUD_TYPE_END: goto loopOut;
                case RGFX_HUD_TYPE_WINDOW:
                    prepare_blank_box();
                    render_blank_box(CURR_ITEM.base.x, CURR_ITEM.base.y, CURR_ITEM.win.x2, CURR_ITEM.win.y2, CURR_ITEM.win.bgColor[0], CURR_ITEM.win.bgColor[1], CURR_ITEM.win.bgColor[2], CURR_ITEM.win.bgColor[3]);
                    finish_blank_box();
                    print_text(CURR_ITEM.base.x + 8, 224 - (CURR_ITEM.base.y + 8), CURR_ITEM.win.title);
                break;
                case RGFX_HUD_TYPE_BUTTON:
                    // find the true x2/y2 by adding the difference of x/y and x2/y2 to the true position of x/y
                    x = get_true_position_x(&CURR_ITEM, 0) + (CURR_ITEM.button.x2 - CURR_ITEM.base.x);
                    y = get_true_position_y(&CURR_ITEM, 0) + (CURR_ITEM.button.y2 - CURR_ITEM.base.y);
                    prepare_blank_box();
                    render_blank_box(get_true_position_x(&CURR_ITEM, 0), get_true_position_y(&CURR_ITEM, 0), x, y, CURR_ITEM.button.bgColor[0], CURR_ITEM.button.bgColor[1], CURR_ITEM.button.bgColor[2], CURR_ITEM.button.bgColor[3]);
                    finish_blank_box();         
                    RGFX_PRINT(get_true_position_x(&CURR_ITEM, 0), get_true_position_y(&CURR_ITEM, 0), CURR_ITEM.button.title)
                    finish_blank_box();         
                break;
                case RGFX_HUD_TYPE_LISTITEM:
                break;
                case RGFX_HUD_TYPE_SPRITE:
                    gSPDisplayList(gDisplayListHead++, dl_hud_img_begin);
                    render_texture_rectangle(get_true_position_x(&CURR_ITEM, 0), get_true_position_y(&CURR_ITEM, 0), CURR_ITEM.sprite.sX, CURR_ITEM.sprite.sY, CURR_ITEM.sprite.tex);
                    gSPDisplayList(gDisplayListHead++, dl_hud_img_end);
                break;
                case RGFX_HUD_TYPE_TEXT:
                    RGFX_PRINT(get_true_position_x(&CURR_ITEM, 0), get_true_position_y(&CURR_ITEM, 0), CURR_ITEM.text.title);
                break;
                case RGFX_HUD_TYPE_NONE:
                break;
            }
        }
        loopOut: (void)0; // "funny ido pacifier"
    }
}

RgfxHud rgfx_end_command() {
    RgfxHud hud;
    hud.base.type = RGFX_HUD_TYPE_END;
    return hud;
}

// nop command

RgfxHud rgfx_none_command() {
    RgfxHud hud;
    hud.base.type = RGFX_HUD_TYPE_NONE;
    return hud;
}

RgfxHud rgfx_create_text(RgfxHud *parent, s16 x, s16 y, char *label) {
    RgfxHud hud;
    hud.base.type = RGFX_HUD_TYPE_TEXT;
    hud.base.selectable = FALSE;
    hud.base.parent = parent;
    hud.button.base.x = x;
    hud.button.base.y = y;
    internal_strcpy(hud.text.title, label);
    return hud;
}

RgfxHud rgfx_create_button(RgfxHud *parent, s16 x, s16 y, char *label) {
    RgfxHud hud;
    hud.base.type = RGFX_HUD_TYPE_BUTTON;
    hud.base.selectable = TRUE;
    hud.base.parent = parent;
    hud.button.base.x = x;
    hud.button.base.y = y;
    hud.button.x2 = x + 32;
    hud.button.y2 = y + 32;
    hud.button.bgColor[0] = 0xFF;
    hud.button.bgColor[1] = 0x00;
    hud.button.bgColor[2] = 0x00;
    hud.button.bgColor[3] = 0xFF;
    internal_strcpy(hud.button.title, label);
    return hud;
}

RgfxHud rgfx_create_window(char *title, s16 x, s16 y, s16 x2, s16 y2, u8 r, u8 g, u8 b, u8 a) {
    RgfxHud hud;
    hud.base.type = RGFX_HUD_TYPE_WINDOW;
    hud.base.parent = NULL;
    hud.base.selectable = FALSE;
    hud.base.x = x;
    hud.base.y = y;

    hud.win.x2 = x2;
    hud.win.y2 = y2;
    hud.win.bgColor[0] = r;
    hud.win.bgColor[1] = g;
    hud.win.bgColor[2] = b;
    hud.win.bgColor[3] = a;
    internal_strcpy(hud.win.title, title);
    return hud;
}

RgfxHud rgfx_create_sprite(RgfxHud *parent, Texture *t, s16 x, s16 y, s16 sX, s16 sY) {
    RgfxHud hud;
    hud.base.type = RGFX_HUD_TYPE_SPRITE;
    hud.base.selectable = FALSE;
    hud.base.parent = parent;
    hud.base.x = x;
    hud.base.y = y;
    hud.sprite.sX = sX;
    hud.sprite.sY = sY;
    hud.sprite.tex = t;
    return hud;
}

// Replacement status HUD.

void rgfx_status_hud() {
    RgfxHud *hud;
    u8 *(*hudLUT)[58] = segmented_to_virtual(&main_hud_lut);
    char buf[16];
    if ((hud = alloc_hud_command(0, 7)) != NULL) {
        hud[0] = rgfx_create_window("", 10, 10, 174, 40, 0x01, 0x01, 0x01, 0);
        hud[1] = rgfx_create_sprite(&hud[0], (*hudLUT)[52], 8, 8, 16, 16);
        hud[2] = rgfx_create_sprite(&hud[0], (*hudLUT)[51], 56, 8, 16, 16);
        hud[3] = rgfx_create_sprite(&hud[0], (*hudLUT)[53], 108, 8, 16, 16);

        sprintf(buf, "x %d", gHudDisplay.lives);
        hud[4] = rgfx_create_text(&hud[1], 18, 2,  buf);
        sprintf(buf, "x %d", gHudDisplay.coins);
        hud[5] = rgfx_create_text(&hud[2], 18, 2, buf);
        sprintf(buf, "x %d", gHudDisplay.stars);
        hud[6] = rgfx_create_text(&hud[3], 18, 2, buf);
    }
}